/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.toolkit.customfield;

import com.atlassian.jira.util.EasyList;
import com.atlassian.jira.toolkit.RequestAttributeKeys;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import junit.framework.TestCase;

import java.util.List;

public class TestMultiIssueKeyComparator extends TestCase
{
    public void testComparatorHasNoState()
    {
        // The comparator must not have state as it gets cached by Lucene and is reused between HTTP requests.
        
        List keys1 = EasyList.build("TST-1", "TST-20", "TST-5", "TST-10", "HSP-1");
        List keys2 = EasyList.build("TST-10", "TST-5", "HSP-100", "TST-20", "HSP-1");

        MultiIssueKeyComparator comparator = new MultiIssueKeyComparator();

        JiraAuthenticationContextImpl.getRequestCache().put(RequestAttributeKeys.MULTIKEY_SEARCHING, keys1);

        assertEquals(1, comparator.compare("TST-10", "TST-20"));

        //start a new request, so clear the cache.
        JiraAuthenticationContextImpl.getRequestCache().clear();

        JiraAuthenticationContextImpl.getRequestCache().put(RequestAttributeKeys.MULTIKEY_SEARCHING, keys2);

        // We have a different request in the thread local now, with a different list of keys. Therefore
        // the result from the compare method should be different.
        assertEquals(-1, comparator.compare("TST-10", "TST-20"));
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
        JiraAuthenticationContextImpl.getRequestCache().clear();
    }
}
