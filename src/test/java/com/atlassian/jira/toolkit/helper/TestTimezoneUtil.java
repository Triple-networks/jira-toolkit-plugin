package com.atlassian.jira.toolkit.helper;

import junit.framework.TestCase;

import java.util.TimeZone;
import java.util.Date;

public class TestTimezoneUtil extends TestCase
{
    public void testGetGMTStringFromTimezoneString()
    {
        if (TimeZone.getTimeZone("Australia/Sydney").inDaylightTime(new Date()))
        {
            assertEquals("GMT+11", TimezoneUtil.getGMTStringFromTimezoneString("Australia/Sydney"));
        }
        else
        {
            assertEquals("GMT+10", TimezoneUtil.getGMTStringFromTimezoneString("Australia/Sydney"));
        }
        assertEquals("GMT+10", TimezoneUtil.getGMTStringFromTimezoneString("GMT+10"));
        assertEquals("GMT+0", TimezoneUtil.getGMTStringFromTimezoneString("GMT"));
        assertEquals("GMT+0", TimezoneUtil.getGMTStringFromTimezoneString("invalid"));
    }
}
