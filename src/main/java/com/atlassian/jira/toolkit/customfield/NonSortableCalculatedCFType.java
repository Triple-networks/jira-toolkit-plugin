package com.atlassian.jira.toolkit.customfield;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Copy / paste from JIRA source with the "implements SortableCustomField" removed.
 *
 * Hopefully in JIRA 6.0 we will make CalculatedCFType not declare implements SortableCustomField
 */
@PublicSpi
public abstract class NonSortableCalculatedCFType<T, S> extends AbstractCustomFieldType<T, S>
{
    public Set<Long> remove(CustomField field)
    {
        return Collections.emptySet();
    }

    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config)
    {
        // Do nothing
    }

    public void createValue(CustomField field, Issue issue, T value)
    {
        // Do nothing
    }

    public void updateValue(CustomField field, Issue issue, T value)
    {
        // Do nothing
    }

    public T getDefaultValue(FieldConfig fieldConfig)
    {
        return null;
    }

    public void setDefaultValue(FieldConfig fieldConfig, T value)
    {
        // Do nothing
    }

    public String getChangelogValue(CustomField field, T value)
    {
        return null;
    }

    public T getValueFromCustomFieldParams(CustomFieldParams parameters) throws FieldValidationException
    {
        Object o = parameters.getFirstValueForKey(null);

        // This method mandates that it cannot be a collection object.  To provide backwards compatibility I have assumed T == S.
        // If not it will break at run time. And hopefully plugin developers will see it.  I'm assuming the method doesn't get used though, cause surely it would have broken before now
        // ParticipantsCustomField is an example of one which is actually a collection.
        return (T) getSingularObjectFromString((String) o);
    }

    public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters)
    {
        return parameters.getFirstValueForNullKey();
    }

    @NotNull
    public List<FieldConfigItemType> getConfigurationItemTypes()
    {
        // No defaults for calculated custom fields and can't be Collections.EMPTY_LIST since it needs to be mutatble
        return new ArrayList<FieldConfigItemType>();
    }

    @Override
    public Object accept(VisitorBase visitor)
    {
        if (visitor instanceof Visitor)
        {
            return ((Visitor) visitor).visitCalculated(this);
        }

        return super.accept(visitor);
    }

    public interface Visitor<X> extends VisitorBase<X>
    {
        X visitCalculated(NonSortableCalculatedCFType calculatedCustomFieldType);
    }
}

