package com.atlassian.jira.toolkit.customfield;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import org.apache.log4j.Logger;

public class EmailDomainNameCFType extends CalculatedCFType
{
    private static final Logger log = Logger.getLogger(EmailDomainNameCFType.class);

    public String getStringFromSingularObject(Object o)
    {
        return (String) o;
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException
    {
        return s;
    }

    public Object getValueFromIssue(CustomField customField, Issue issue)
    {
        try
        {
            User user;
            if ("assignee".equals(getDescriptor().getParams().get("role")))
            {
                user = issue.getAssignee();
            }
            else
            {
                user = issue.getReporter();
            }
            if (user != null)
            {
                String email = user.getEmailAddress();
                if (email != null && email.indexOf('@') > -1)
                {
                    return email.substring(email.indexOf('@') + 1, email.length());
                }
            }
        }
        catch (Exception e)
        {
            log.warn(e.getMessage(), e);
        }

        return "";

    }
}
